import string
from django.conf import settings
from math import floor


def to_base62(num: int, b=62):
    '''Convert a num to base62'''
    if b <= 0 or b > 62:
        return 0
    base = string.digits + string.ascii_lowercase + string.ascii_uppercase
    r = num % b
    res = base[r]
    q = floor(num / b)
    while q:
        r = q % b
        q = floor(q / b)
        res = base[int(r)] + res
    return res


def to_hash(num: int, user_hash: string, b=62):
    '''Convert user_hash string based on a number to base62'''
    if b <= 0 or b > 62:
        return 0
    base = string.digits + user_hash.lower()*len(user_hash) + user_hash.upper()*len(user_hash)
    r = num % b
    res = base[r]
    q = floor(num / b)
    while q:
        r = q % b
        q = floor(q / b)
        res = base[int(r)] + res
    return res


def get_shortened(url: string, user_hash: string) -> string:
    '''Create shortened URL based on user request.'''
    redis_con = settings.REDIS_CON
    if user_hash:
        shortened = to_hash(int(redis_con.get('COUNTER_USER')), user_hash)
        redis_con.incr('COUNTER_USER')
    else:
        shortened = to_base62(int(redis_con.get('COUNTER_HASH')))
        redis_con.incr('COUNTER_HASH')
    redis_con.set(shortened, url)
    return shortened
