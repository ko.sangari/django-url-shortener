from rest_framework.authentication import SessionAuthentication


class SessionAuthenticationCsrfExempt(SessionAuthentication):
    '''Customize the SessionAuthentication to Exempt CSRF'''
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening
