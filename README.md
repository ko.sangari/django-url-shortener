## Technologies
- DRF
- Django-AllAuth
- Redis
- Celery
- Swagger


## Requirements
* Docker
* Docker Compose

### Note : copy `.env.example` to `.env` and change enviornment variables if needed.
```bash
$ cp .env.example .env
```
### Running the project
- run command below:
```bash
$ docker-compose up -d
```
Now, your docker ctainers are running. to see containers list just run:
```bash
$ docker ps
```
