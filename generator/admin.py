from django.contrib import admin

from .models import URL, AccessLog


@admin.register(URL)
class URLAdmin(admin.ModelAdmin):
    list_display = ('create_at', 'user', 'url', 'shortened')
    ordering = ['id', ]


@admin.register(AccessLog)
class AccessLogAdmin(admin.ModelAdmin):
    list_display = ('create_at', 'url', 'user_ip', 'browser')
    ordering = ['create_at', ]
