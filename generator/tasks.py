from celery import shared_task

from generator.models import URL, AccessLog


@shared_task(name='shortner.log_url_request')
def log_url_request(user_ip: str, url: int, is_mobile: bool, is_tablet: bool,
                    is_touch_capable: bool, is_pc: bool, is_bot: bool,
                    browser: str, browser_version: str, os: str,
                    os_version: str, device: str) -> None:
    url = URL.objects.get(id=url)
    AccessLog.objects.create(
        user_ip=user_ip,
        url=url,
        is_mobile=is_mobile,
        is_tablet=is_tablet,
        is_touch_capable=is_touch_capable,
        is_pc=is_pc,
        is_bot=is_bot,
        browser=browser,
        browser_version=browser_version,
        os=os,
        os_version=os_version,
        device=device
    )
