from datetime import datetime, date, timedelta
from django.conf import settings
from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated

from .models import URL, AccessLog
from .serializers import URLSerializer
from .tasks import log_url_request
from utilities.get_client_ip import get_client_ip
from utilities.get_shortened import get_shortened


class GenerateView(APIView):
    '''
    Get the Orginal URL (and a user_hash string fo suggestion)
    and return shortened URL.
    '''
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = URLSerializer
        serializer = serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        shortened = get_shortened(serializer.validated_data['url'],
                                  serializer.validated_data.get('user_hash'))

        URL.objects.create(
            user=request.user,
            url=serializer.validated_data['url'],
            shortened=shortened)
        base_url = request.build_absolute_uri().replace(request.get_full_path(), '')
        return Response({'shortened_url': base_url+'/r/'+shortened}, status=status.HTTP_200_OK)


class RedirectView(APIView):
    '''Get the shortened URL and redirect user to Orginal URL.'''
    permission_classes = (AllowAny,)

    def get(self, request, shortened):
        redis_con = settings.REDIS_CON
        user_ip = get_client_ip(request)
        try:
            url = URL.objects.get(shortened=shortened)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        log_url_request.delay(
                            user_ip=user_ip,
                            url=url.id,
                            is_mobile=request.user_agent.is_mobile,
                            is_tablet=request.user_agent.is_tablet,
                            is_touch_capable=request.user_agent.is_touch_capable,
                            is_pc=request.user_agent.is_pc,
                            is_bot=request.user_agent.is_bot,
                            browser=request.user_agent.browser.family,
                            browser_version=request.user_agent.browser.version_string,
                            os=request.user_agent.os.family,
                            os_version=request.user_agent.os.version_string,
                            device=request.user_agent.device.family)
        return HttpResponseRedirect(redis_con.get(shortened))


class ReportTodayView(APIView):
    '''Return report of user URL's for Today.'''
    permission_classes = [IsAuthenticated]

    def get(self, request):
        time_start = datetime.now().replace(hour=0, minute=0, second=0)
        time_end = datetime.now()
        query_list = URL.objects.filter(
                                user=request.user,
                                create_at__range=[time_start, time_end]
                            )
        result_total, result_per_user = report_generator(query_list)
        return Response({
                            'result_total': result_total,
                            'result_per_user': result_per_user
                        },
                        status=status.HTTP_200_OK)


class ReportYesterdayView(APIView):
    '''Return report of user URL's for Yesterday.'''
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        yesterday = date.today() - timedelta(days=1)
        query_list = URL.objects.filter(
                                        user=request.user,
                                        create_at__date=yesterday)
        result_total, result_per_user = report_generator(query_list)
        return Response({
                            'result_total': result_total,
                            'result_per_user': result_per_user
                        },
                        status=status.HTTP_200_OK)


class ReportLastWeekView(APIView):
    '''Return report of user URL's for Last 7 Days.'''
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        yesterday = date.today() - timedelta(days=7)
        query_list = URL.objects.filter(
                                        user=request.user,
                                        create_at__date=yesterday)
        result_total, result_per_user = report_generator(query_list)
        return Response({
                            'result_total': result_total,
                            'result_per_user': result_per_user
                        },
                        status=status.HTTP_200_OK)


class ReportLastMonthView(APIView):
    '''Return report of user URL's for Last 30 Days.'''
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        yesterday = date.today() - timedelta(days=30)
        query_list = URL.objects.filter(
                                        user=request.user,
                                        create_at__date=yesterday)
        result_total, result_per_user = report_generator(query_list)
        return Response({
                            'result_total': result_total,
                            'result_per_user': result_per_user
                        },
                        status=status.HTTP_200_OK)


def report_generator(query_list):
    '''Main function for generating reports.'''
    result_total = list()
    for item in query_list:
        total_visit = AccessLog.objects.filter(url=item).count()
        visit_by_desktop = AccessLog.objects.filter(
                                url=item, is_pc=True).count()
        visit_by_mobile = AccessLog.objects.filter(
                                url=item, is_mobile=True).count()
        visit_by_browser = AccessLog.objects.filter(
                                url=item).values('browser').annotate(
                                    count=Count('browser'))
        result_dict = {
            "url": item.url,
            "total_visit": total_visit,
            "visit_by_desktop": visit_by_desktop,
            "visit_by_mobile": visit_by_mobile,
            "visit_by_browser": visit_by_browser
        }
        result_total.append(result_dict)

    result_per_user = list()
    for item in query_list:
        for user in AccessLog.objects.filter(
                        url=item).values('user_ip').annotate(
                            count=Count('user_ip')):
            total_visit = user['count']
            visit_by_desktop = AccessLog.objects.filter(
                                    url=item,
                                    user_ip=user['user_ip'],
                                    is_pc=True).count()
            visit_by_mobile = AccessLog.objects.filter(
                                    url=item,
                                    user_ip=user['user_ip'],
                                    is_mobile=True).count()
            visit_by_browser = AccessLog.objects.filter(
                                    url=item,
                                    user_ip=user['user_ip']
                                ).values('browser').annotate(
                                    count=Count('browser'))
            result_dict = {
                "url": item.url,
                "user": user,
                "total_visit": total_visit,
                "visit_by_desktop": visit_by_desktop,
                "visit_by_mobile": visit_by_mobile,
                "visit_by_browser": visit_by_browser
            }
            result_per_user.append(result_dict)

    return result_total, result_per_user
