from django.db import models
from django.contrib.auth.models import User


class URL(models.Model):
    create_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    url = models.CharField(max_length=256)
    shortened = models.CharField(max_length=256)

    def __str__(self):
        return self.url


class AccessLog(models.Model):
    create_at = models.DateTimeField(auto_now_add=True)
    url = models.ForeignKey('URL', on_delete=models.CASCADE, null=True)
    user_ip = models.CharField(max_length=32, default='')
    is_mobile = models.BooleanField(default=False)
    is_tablet = models.BooleanField(default=False)
    is_touch_capable = models.BooleanField(default=False)
    is_pc = models.BooleanField(default=False)
    is_bot = models.BooleanField(default=False)
    browser = models.CharField(max_length=32, default='')
    browser_version = models.CharField(max_length=32, default='')
    os = models.CharField(max_length=32, default='')
    os_version = models.CharField(max_length=32, default='')
    device = models.CharField(max_length=32, default='')

    def __str__(self):
        return self.url.url
