from rest_framework import serializers


class URLSerializer(serializers.Serializer):
    url = serializers.CharField(max_length=256, write_only=True)
    user_hash = serializers.CharField(
                    max_length=256, write_only=True, required=False)
    short = serializers.CharField(max_length=256, read_only=True)
