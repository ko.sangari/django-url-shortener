from django.urls import path, include
from . import views

urlpatterns = [
    path('convert/', views.GenerateView.as_view()),
    path('report/', include([
        path('today/', views.ReportTodayView.as_view()),
        path('yesterday/', views.ReportYesterdayView.as_view()),
        path('lastweek/', views.ReportLastWeekView.as_view()),
        path('lastmonth/', views.ReportLastMonthView.as_view()),
    ])),
]
