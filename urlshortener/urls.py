from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from generator.views import RedirectView


urlpatterns = [
    path('accounts/', include('allauth.urls')),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('admin/', admin.site.urls),
    path('api/', include([
        path('', include('generator.urls')),
    ])),
    path('r/<shortened>', RedirectView.as_view()),
]

if settings.DRF_ENABLED:
    schema_view = get_schema_view(
        openapi.Info(
            title="URLShortener API",
            default_version='v1',
        ),
        permission_classes=(permissions.AllowAny,),
        patterns=urlpatterns,
    )
    schema = schema_view.with_ui('swagger')

    urlpatterns += [
        path('swagger/', schema),
    ]
